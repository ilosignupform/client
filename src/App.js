import './App.css'

import SignUpForm from './components/SignUpForm'
import { ThemeProvider } from '@mui/material/styles'
import theme from './theme/Theme'

// import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3'

function App () {
  return (
    <ThemeProvider theme={theme}>
      {/*
      <GoogleReCaptchaProvider
        reCaptchaKey={process.env.REACT_APP_RECAPTCHA_KEY}
      />
      */}
      <div className='App'>
        <header className='App-header'>
          <SignUpForm />
        </header>
      </div>
    </ThemeProvider>
  )
}

export default App
