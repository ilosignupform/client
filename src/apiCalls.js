export async function submitInfo (data) {
  const response = await window.fetch(`${process.env.REACT_APP_SERVER_URL}/submit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    },
    body: JSON.stringify(data)
  })
  return response.json()
}
