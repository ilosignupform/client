import React from 'react'
import { Button, ButtonGroup } from '@mui/material'

import BoxHeader from './BoxHeader'

function Employees ({ nextStep, handleChange }) {
  const onClick = (e) => {
    handleChange(e)
    nextStep()
  }
  return (
    <>
      <BoxHeader />
      <h2>How many employees do you have?</h2>
      <ButtonGroup>
        <Button variant='outlined' color='secondary' onClick={onClick} value='< 5'>Less than 5 </Button>
        <Button variant='outlined' color='secondary' onClick={onClick} value='5-49'>5 - 49</Button>
        <Button variant='outlined' color='secondary' onClick={onClick} value='49 <'>More than 49</Button>
      </ButtonGroup>
    </>
  )
}

export default Employees
