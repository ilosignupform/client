import React from 'react'

import BoxHeader from './BoxHeader'

function SorryToHear () {
  return (
    <>
      <BoxHeader />
      <p style={{ color: '#FA3C4B', marginTop: '40px' }}>We are sorry to hear that. If you know any women entrepreneurs who might be interested, please spread the word.</p>
    </>
  )
}

export default SorryToHear
