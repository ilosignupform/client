import React from 'react'

import BoxHeader from './BoxHeader'

function NotWithinCriteria () {
  return (
    <>
      <BoxHeader />
      <p style={{ color: '#FA3C4B', marginTop: '40px' }}>Unfortunatly your bussiness doesn't meet this project's criterias. Thank you for your interest.</p>
    </>
  )
}

export default NotWithinCriteria
