import React, { useState, useEffect } from 'react'
import { Button, ButtonGroup } from '@mui/material'

import BoxHeader from './BoxHeader'

function Revenue ({ nextStep, handleChange, country, fail }) {
  const [currencyText1, setCurrencyText1] = useState()
  const [currencyText2, setCurrencyText2] = useState()

  useEffect(() => {
    switch (country) {
      case 'Malaysia':
        setCurrencyText1('Less than RM 5 million')
        setCurrencyText2('More than RM 5 million')
        break
      case 'Philippines':
        setCurrencyText1('Less than ₱ 75 million')
        setCurrencyText2('More than ₱ 75 million')
        break
      case 'Thailand':
        setCurrencyText1('Less than ฿ 50 million')
        setCurrencyText2('More than ฿ 50 million')
    }
  }, [country])

  const onClick = (e) => {
    handleChange(e)
    nextStep()
  }

  return (
    <>
      <BoxHeader />
      <h2>What is your annual revenue?</h2>
      <ButtonGroup>
        <Button variant='outlined' color='secondary' onClick={onClick} value='< $1500000'>{currencyText1}</Button>
        <Button variant='outlined' color='secondary' onClick={onClick} value='$1500000 <'>{currencyText2}</Button>
      </ButtonGroup>
    </>
  )
}

export default Revenue
