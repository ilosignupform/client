import React from 'react'

import logo from './iloLogo.svg'

function Success () {
  return (
    <>
      <img src={logo} className='App-logo' alt='logo' />
      <h1>Thank you! We will contact you with more information.</h1>
    </>
  )
}

export default Success
