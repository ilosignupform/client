import React from 'react'
import logo from './iloLogo.svg'
import { IoMdPlay } from 'react-icons/io'

function BoxHeader () {
  const redirectToILO = () => {
    window.location.href = 'https://learninghub.ilo.org/home'
  }

  return (
    <div>
      <img src={logo} className='App-logo' alt='logo' onClick={redirectToILO} />
      <h1>Are you a female entrepreneur?</h1>
      <p>The ILO’s Rebuilding Better project is looking for women entrepreneurs in Malaysia, Philippines and Thailand. We are providing support to women entrepreneurs whose businesses have been impacted by COVID-19 by providing access to training, financial support, market and networking opportunities.</p>
      <a href='https://www.ilo.org/asia/projects/WCMS_767653/lang--en/index.htm#:~:text=The%20Rebuilding%20Better%20project%2C%20supported,access%20to%20vital%20support%20services.'> <IoMdPlay className='link-icon' color='#FA3C4B' />Read more about the project</a>
    </div>
  )
}

export default BoxHeader
