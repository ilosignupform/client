import React, { useState } from 'react'
import Interested from './Interested'
import Country from './Country'
import Owners from './Owners'
import Employees from './Employees'
import Revenue from './Revenue'
import Container from '@mui/material/Container'
import Box from '@mui/material/Box'
import { submitInfo } from '../apiCalls'
import NotWithinCriteria from './NotWithinCriteria'
import ContactDetails from './ContactDetails'
import Success from './Success'
import SorryToHear from './SorryToHear'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

function SignUpForm () {
  const [step, setStep] = useState(1)
  const [country, setCountry] = useState('')
  const [isFemaleOwners, setIsFemaleOwners] = useState('false')
  const [employees, setEmployees] = useState('')
  const [revenue, setRevenue] = useState('')

  const [name, setName] = useState('')
  const [company, setCompany] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')

  const [error, setError] = useState('')

  const [isLoading, setIsLoading] = useState(false)

  const nextStep = () => {
    setStep(step + 1)
  }

  const notWithinCriteria = () => {
    setStep(9)
  }

  const sorryToHear = () => {
    setStep(8)
  }

  const handleChange = input => event => {
    input(event.target.value)
  }

  const notify = (message) => toast(message)

  const handleSubmit = async (event) => {
    event.preventDefault()
    setIsLoading(true)
    const body = {
      name: name,
      company: company,
      email: email,
      phone: phone,

      country: country,
      employees: employees,
      revenue: revenue
    }

    const response = await submitInfo(body)
    if (response.errors) {
      setError(response.errors)
      response.errors.forEach(error => {
        notify(`Invalid ${error.param}`)
      })
    } else {
      setStep(7)
    }
    setIsLoading(false)
  }

  const renderStep = () => {
    switch (step) {
      case 1:
        return <Interested nextStep={nextStep} sorryToHear={sorryToHear} />
      case 2:
        return <Country nextStep={nextStep} handleChange={handleChange(setCountry)} />
      case 3:
        return <Owners nextStep={nextStep} handleChange={handleChange(setIsFemaleOwners)} fail={notWithinCriteria} />
      case 4:
        return <Employees nextStep={nextStep} handleChange={handleChange(setEmployees)} />
      case 5:
        return <Revenue nextStep={nextStep} handleChange={handleChange(setRevenue)} country={country} />
      case 6:
        return <ContactDetails
          nextStep={nextStep}
          handleNameChange={handleChange(setName)}
          handleCompanyChange={handleChange(setCompany)}
          handleEmailChange={handleChange(setEmail)}
          handlePhoneChange={handleChange(setPhone)}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          handleSubmit={handleSubmit}

          name={name}
          company={company}
          email={email}
          phone={phone}

          error={error}
               />
      case 7:
        return <Success />
      case 8:
        return <SorryToHear />
      case 9:
        return <NotWithinCriteria />
    }
  }

  return (
    <>
      <ToastContainer />
      <Container maxWidth='sm'>
        <Box>
          {renderStep()}
        </Box>
      </Container>
    </>
  )
}

export default SignUpForm
