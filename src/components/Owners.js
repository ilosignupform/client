import React from 'react'
import Button from '@mui/material/Button'

import BoxHeader from './BoxHeader'

function Owners ({ nextStep, handleChange, fail }) {
  const onClick = (e) => {
    handleChange(e)
    nextStep()
  }
  return (
    <>
      <BoxHeader />
      <h2>Do women own 51% or more of your business?</h2>
      <Button variant='outlined' color='secondary' onClick={onClick} style={{ margin: '0 5px' }} value='true'>Yes </Button>
      <Button variant='outlined' color='secondary' onClick={fail} style={{ margin: '0 5px' }}>No</Button>
    </>
  )
}

export default Owners
