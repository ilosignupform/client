import React from 'react'
import { TextField, FormControl, Button, CircularProgress } from '@mui/material'

import logo from './iloLogo.svg'

function ContactDetails ({ nextStep, handleNameChange, handleCompanyChange, handleEmailChange, handlePhoneChange, isLoading, handleSubmit, name, company, email, phone }) {
  return (
    <>
      <img src={logo} className='App-logo' alt='logo' />
      <h1>Are you a female entrepreneur?</h1>
      <h2>Contact details</h2>
      <FormControl sx={{ width: '26ch' }}>
        <TextField
          type='text'
          label='Name'
          fullWidth
          required
          onChange={handleNameChange}
          margin='dense'
          autoFocus
        />
        <TextField
          type='text'
          label='Company name'
          fullWidth
          required
          onChange={handleCompanyChange}
          margin='dense'
        />
        <TextField
          type='email'
          label='Email'
          value={email}
          fullWidth
          required
          onChange={handleEmailChange}
          margin='dense'
        />
        <TextField
          type='text'
          label='Phone'
          fullWidth
          required
          onChange={handlePhoneChange}
          margin='dense'
        />
        <Button
          type='submit'
          disabled={!name || !company || !email || !phone || isLoading}
          variant='outlined'
          color='secondary'
          style={{ marginTop: '15px' }}
          onClick={handleSubmit}
        >
          {isLoading
            ? <CircularProgress className='loader' color='secondary' size='24px' />
            : 'Submit'}
        </Button>
      </FormControl>
    </>
  )
}

export default ContactDetails
