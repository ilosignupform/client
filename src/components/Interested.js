import React from 'react'
import Button from '@mui/material/Button'
import BoxHeader from './BoxHeader'

function Interested ({ nextStep, sorryToHear }) {
  return (
    <>
      <BoxHeader />
      <h2>Are you a women entrepreneur and interested in learning more about our upcoming activities?</h2>
      <Button variant='outlined' color='secondary' onClick={nextStep} style={{ margin: '0 5px' }}>Yes </Button>
      <Button variant='outlined' color='secondary' onClick={sorryToHear} style={{ margin: '0 5px' }}>No</Button>
    </>
  )
}

export default Interested
