import React from 'react'
import { Button, ButtonGroup } from '@mui/material'

import BoxHeader from './BoxHeader'

function Country ({ nextStep, handleChange }) {
  const onClick = (e) => {
    handleChange(e)
    nextStep()
  }
  return (
    <>
      <BoxHeader />
      <h2>What country are you operating in?</h2>
      <ButtonGroup>
        <Button variant='outlined' color='secondary' onClick={onClick} value='Malaysia'>Malaysia </Button>
        <Button variant='outlined' color='secondary' onClick={onClick} value='Philippines'>Philippines</Button>
        <Button variant='outlined' color='secondary' onClick={onClick} value='Thailand'>Thailand</Button>
      </ButtonGroup>
    </>
  )
}

export default Country
